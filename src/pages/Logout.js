import { useContext, useEffect } from 'react';

import { Redirect } from 'react-router-dom';

import GlobalDataContext from '../GlobalDataContext';

export default function Logout () {
		
	const { setUser, unsetUser } = useContext(GlobalDataContext)

	// useEffect is already declared in App.js
	unsetUser();

	useEffect(() =>{
		setUser( {customerAccessToken: null} )
	}, [])


	return (
			//Redirect back to login
			< Redirect to='/login' />
		)

} 